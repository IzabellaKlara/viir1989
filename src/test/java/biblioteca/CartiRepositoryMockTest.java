package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repositoryMock.CartiRepositoryMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class CartiRepositoryMockTest {
    private CartiRepositoryMock repository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void cautaCarte_TC01() throws Exception {
        List<Carte> cartiGasite = repository.cautaCarte("ion");
        assertTrue(cartiGasite.size() == 0);
    }

    @Test
    public void cautaCarte_TC02() throws Exception {
        repository.adaugaCarte(Carte.getCarteFromString("Poezii;;1973;Corint;poezii"));
        List<Carte> cartiGasite = repository.cautaCarte("test");
        assertTrue(cartiGasite.size() == 0);
    }

    @Test
    public void cautaCarte_TC03() throws Exception {
        repository.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        List<Carte> cartiGasite = repository.cautaCarte("ion");
        assertTrue(cartiGasite.size() == 1);
    }

    @Test
    public void cautaCarte_TC04() throws Exception {
        repository.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        List<Carte> cartiGasite = repository.cautaCarte("test");
        assertTrue(cartiGasite.size() == 0);
    }

    @Test
    public void cautaCarte_TC05() throws Exception {
        repository.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        repository.adaugaCarte(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
        List<Carte> cartiGasite = repository.cautaCarte("ion");
        assertTrue(cartiGasite.size() == 1);
    }
    @Before
    public void setUp() throws Exception {
        repository = new CartiRepositoryMock();
        repository.carti = new ArrayList<Carte>();


    }
}
