package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repositoryMock.CartiRepositoryMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TopDownTest {
    private CartiRepositoryMock repository;
    private CartiController controller;
    private Carte carte1;
    private Carte carte2;

    private List<String> autori;
    private List<String> cuvinte;

    private String editura;

    private String an;

    private String titlu;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void adaugaCarte() throws Exception {
        int numarCarti = repository.getCarti().size();
        controller.adaugaCarte(carte1);
        assertTrue(repository.getCarti().size() == numarCarti+1);
    }

    @Test
    public void cautaCarte() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("Valoare invalida pentru an aparitie!");
        controller.adaugaCarte(carte2);
        List<Carte> cartiGasite = controller.cautaCarte("test");
        assertTrue(cartiGasite.size() == 0);

    }



    @Test
    public void getCartiDinAnul() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("Valoare invalida pentru an aparitie!");
        controller.adaugaCarte(carte2);
        List<Carte> cartiGasite = controller.cautaCarte("mateiu");
        assertTrue(cartiGasite.size() == 1);
        List<Carte> cartiGasiteDinAnul = controller.getCartiOrdonateDinAnul("1899");
        assertTrue(cartiGasiteDinAnul.size() == 0);
    }




    @Before
    public void setUp() throws Exception {
        repository = new CartiRepositoryMock();
        controller = new CartiController(repository);
        autori = new ArrayList<String>();
        cuvinte = new ArrayList<String>();

        autori.add("author");
        cuvinte.add("keyword");

        editura = "Corint";
        titlu = "test";
        an = "2010";

        carte1 = new Carte();
        carte1.setTitlu(titlu);
        carte1.setAutori(autori);
        carte1.setEditura(editura);
        carte1.setCuvinteCheie(cuvinte);
        carte1.setAnAparitie(an);

        an="1899";
        titlu = "test";
        carte2 = new Carte();
        carte2.setTitlu(titlu);
        carte2.setAutori(autori);
        carte2.setEditura(editura);
        carte2.setCuvinteCheie(cuvinte);
        carte2.setAnAparitie(an);

    }
}
