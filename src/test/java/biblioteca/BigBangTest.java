package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repositoryMock.CartiRepositoryMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class BigBangTest {
    private CartiRepositoryMock repository;
    private CartiController controller;
    private Carte carte1;
    private Carte carte2;
    private Carte carte3;
    private Carte carte4;

    private List<String> autori;
    private List<String> cuvinte;

    private String editura;

    private String an;

    private String titlu;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void adaugaCarte() throws Exception {
        int numarCarti = repository.getCarti().size();
        controller.adaugaCarte(carte1);
        assertTrue(repository.getCarti().size() == numarCarti+1);
    }

    @Test
    public void cautaCarte() throws Exception {
        List<Carte> cartiGasite = controller.cautaCarte("ion");
        assertTrue(cartiGasite.size() == 2);
    }



    @Test
    public void cautaCartiDinAn() throws Exception {
        List<Carte> cartiGasite = controller.getCartiOrdonateDinAnul("1948");
        assertTrue(cartiGasite.size() == 3);
    }


    @Test
    public void bigBang() throws Exception {
        int numarCarti = repository.getCarti().size();
        controller.adaugaCarte(carte1);
        assertTrue(repository.getCarti().size() == numarCarti +1);
        List<Carte> cartiGasite = controller.cautaCarte("author");
        assertTrue(cartiGasite.size() == 1);
        List<Carte> cartiGasiteDinAnul = controller.getCartiOrdonateDinAnul("2010");
        assertTrue(cartiGasiteDinAnul.size() == 1);
    }

    @Before
    public void setUp() throws Exception {
        repository = new CartiRepositoryMock();
        controller = new CartiController(repository);
        autori = new ArrayList<String>();
        cuvinte = new ArrayList<String>();

        autori.add("author");
        cuvinte.add("keyword");

        editura = "Corint";
        titlu = "test";
        an = "2010";

        carte1 = new Carte();
        carte1.setTitlu(titlu);
        carte1.setAutori(autori);
        carte1.setEditura(editura);
        carte1.setCuvinteCheie(cuvinte);
        carte1.setAnAparitie(an);

//        titlu = "";
//        for(int i = 0;i<255;i++){
//            titlu = titlu + "a";
//        }
//        carte2 = new Carte();
//        carte2.setTitlu(titlu);
//        carte2.setAutori(autori);
//        carte2.setEditura(editura);
//        carte2.setCuvinteCheie(cuvinte);
//        carte2.setAnAparitie(an);
//
//        titlu = "";
//        carte3 = new Carte();
//        carte3.setTitlu(titlu);
//        carte3.setAutori(autori);
//        carte3.setEditura(editura);
//        carte3.setCuvinteCheie(cuvinte);
//        carte3.setAnAparitie(an);
//
//        an="1899";
//        titlu = "test";
//        carte4 = new Carte();
//        carte4.setTitlu(titlu);
//        carte4.setAutori(autori);
//        carte4.setEditura(editura);
//        carte4.setCuvinteCheie(cuvinte);
//        carte4.setAnAparitie(an);

    }
}
