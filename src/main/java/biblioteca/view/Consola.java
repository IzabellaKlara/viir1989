package biblioteca.view;


import biblioteca.controller.CartiController;
import biblioteca.model.Carte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Consola {

	private BufferedReader bufferedReader;
	CartiController cartiController;
	
	public Consola(CartiController cartiController){
		this.cartiController = cartiController;
	}
	
	public void executa() throws IOException {
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		int optiune = -1;
		while(optiune!=0){
			switch(optiune){
				case 1:
					adaugaCarte();
					break;
				case 2:
					cautaCartiDupaAutor();
					break;
				case 3:
					afiseazaCartiOrdonateDinAnul();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti o optiune:");
				line= bufferedReader.readLine();
			}while(!line.matches("[0-3]"));
			optiune=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.println("\n");
		System.out.println("Gestiunea cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     0. Exit");
	}
	
	public void adaugaCarte(){
		Carte carte = new Carte();
		try{
			System.out.println("\n");
			System.out.println("Titlu:");
			carte.setTitlu(bufferedReader.readLine());
			String line;
			do{
				System.out.println("An aparitie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[10-9]+"));
			carte.setAnAparitie(line);
			do{
				System.out.println("Nr. de autori:");
				line= bufferedReader.readLine();
			}while(!line.matches("[1-9]+"));
			int nrAutori= Integer.parseInt(line);
			for(int i=1;i<=nrAutori;i++){
				System.out.println("Autor "+i+": ");
				carte.adaugaAutor(bufferedReader.readLine());
			}
			do{
				System.out.println("Nr. de cuvinte cheie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[1-9]+"));
			int nrCuvinteCheie= Integer.parseInt(line);
			for(int i=1;i<=nrCuvinteCheie;i++){
				System.out.println("Cuvant "+i+": ");
				carte.adaugaCuvantCheie(bufferedReader.readLine());
			}
			cartiController.adaugaCarte(carte);
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void cautaCartiDupaAutor(){
		System.out.println("\n");
		System.out.println("Autor:");
		try {
			for(Carte c: cartiController.cautaCarte(bufferedReader.readLine())){
				System.out.println(c);
			}
		}
		 catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void afiseazaCartiOrdonateDinAnul(){
		System.out.println("\n");
		try{
			String line;
			do{
				System.out.println("An aparitie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[10-9]+"));
			for(Carte c: cartiController.getCartiOrdonateDinAnul(line)){
				System.out.println(c);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
