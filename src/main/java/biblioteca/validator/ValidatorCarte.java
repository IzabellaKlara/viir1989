package biblioteca.validator;

import biblioteca.model.Carte;

public class ValidatorCarte {
	
	public static boolean isStringOK(String string) throws Exception{
		boolean flag = string.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte carte)throws Exception{
		if(carte.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie nula!");
		}
		if(carte.getCuvinteCheie().size()==0){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(carte.getTitlu().length() == 1){
			throw new Exception("Titlul trebuie sa aiba lungime mai mare decat 1!");
		}
		if(carte.getEditura().length() == 1){
			throw new Exception("Editura trebuie sa aiba lungime mai mare decat 1!");
		}
		if(carte.getTitlu().length() > 255){
			throw new Exception("Titlul trebuie sa aiba lungime mai mica decat 255!");
		}
		if(Integer.parseInt(carte.getAnAparitie()) > 2018 || Integer.parseInt(carte.getAnAparitie()) < 1900 ){
			throw new Exception("Valoare invalida pentru an aparitie!");
		}
		if(carte.getEditura().length() > 255){
			throw new Exception("Editura trebuie sa aiba lungime mai mica decat 255!");
		}
		if(carte.getAutori()==null){
			throw new Exception("Lista autori nula!");
		}
		if(carte.getAutori().size()==0){
			throw new Exception("Lista autori nula!");
		}
		if(!isOKString(carte.getTitlu()))
			throw new Exception("Titlu invalid!");
		for(String s:carte.getAutori()){
			if(!isOKString(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:carte.getCuvinteCheie()){
			if(!isOKString(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!ValidatorCarte.isNumber(carte.getAnAparitie()))
			throw new Exception("An invalid!");
	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}
	
	public static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		else if(s.length() == 0)
		{
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
