package biblioteca.repository.repositoryMock;


import biblioteca.model.Carte;
import biblioteca.repository.repositoryInterfaces.CartiRepositoryInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepositoryMock implements CartiRepositoryInterface {

	public List<Carte> carti;
	
	public CartiRepositoryMock(){
		carti = new ArrayList<Carte>();
		
		carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
		carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
		carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
		carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
		carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
		carti.add(Carte.getCarteFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));

	}
	
	@Override
	public void adaugaCarte(Carte carte) {
		carti.add(carte);
	}
	
	@Override
	public List<Carte> cautaCarte(String autor) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<carti.size()){
			boolean flag = false;
			List<String> listaAutori = carti.get(i).getAutori();
			int j = 0;
			while(j<listaAutori.size()){
				if(listaAutori.get(j).toLowerCase().contains(autor.toLowerCase())){
					flag = true;
					break;
				}
				j++;
			}
			if(flag == true){
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	public List<Carte> getCarti() {
		return carti;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) {
		List<Carte> listaCarti = getCarti();
		List<Carte> listaCartiSortata = new ArrayList<Carte>();
		for(Carte c:listaCarti){
			if(c.getAnAparitie().equals(an) == true){
				listaCartiSortata.add(c);
			}
		}
		Collections.sort(listaCartiSortata,new Comparator<Carte>(){
			@Override
			public int compare(Carte primaCarte, Carte aDouaCarte) {
				if(primaCarte.getTitlu().compareTo(aDouaCarte.getTitlu())==0){
					return primaCarte.getAutori().get(0).compareTo(aDouaCarte.getAutori().get(0));
				}
				return primaCarte.getTitlu().compareTo(aDouaCarte.getTitlu());
			}
		});
		
		return listaCartiSortata;
	}

}
