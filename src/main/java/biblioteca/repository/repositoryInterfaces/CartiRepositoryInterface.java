package biblioteca.repository.repositoryInterfaces;
import biblioteca.model.Carte;
import java.util.List;

public interface CartiRepositoryInterface {
	void adaugaCarte(Carte c);
	List<Carte> cautaCarte(String ref);
	List<Carte> getCartiOrdonateDinAnul(String an);
}
