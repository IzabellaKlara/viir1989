package biblioteca.repository.repository;


import biblioteca.model.Carte;
import biblioteca.repository.repositoryInterfaces.CartiRepositoryInterface;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepository implements CartiRepositoryInterface {
	
	private String file = "out/cartiBD.txt";
	
	public CartiRepository(){
		URL location = CartiRepository.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
	}
	
	@Override
	public void adaugaCarte(Carte c) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(c.toString());
			bw.newLine();
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Carte> getCarti() {
		List<Carte> lc = new ArrayList<Carte>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				lc.add(Carte.getCarteFromString(line));
			}

			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lc;
	}

	@Override
	public List<Carte> cautaCarte(String autor) {
		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		while (i<carti.size()){
			boolean flag = false;
			List<String> autori = carti.get(i).getAutori();
			int j = 0;
			while(j<autori.size()){
				if(autori.get(j).toLowerCase().contains(autor.toLowerCase())){
					flag = true;
					break;
				}
				j++;
			}
			if(flag == true){
				cartiGasite.add(carti.get(i));
			}
			i++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(String an) {
		List<Carte> listaCarti = getCarti();
		List<Carte> listaCartiSortate = new ArrayList<Carte>();
		for(Carte c:listaCarti){
			if(c.getAnAparitie().equals(an) == false){
				listaCartiSortate.add(c);
			}
		}
		
		Collections.sort(listaCartiSortate,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					return a.getAutori().get(0).compareTo(b.getAutori().get(0));
				}
				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		return listaCartiSortate;
	}

}
