package biblioteca.begin;

import biblioteca.controller.CartiController;
import biblioteca.repository.repositoryInterfaces.CartiRepositoryInterface;
import biblioteca.repository.repositoryMock.CartiRepositoryMock;
import biblioteca.view.Consola;

import java.io.IOException;

//functionalitati
//i.	 adaugarea unei noi carti (titlu, autori, an aparitie, editura, cuvinte cheie);
//ii.	 cautarea cartilor scrise de un anumit autor (sau parti din numele autorului);
//iii.	 afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori.



public class Start {
	
	public static void main(String[] args) {
		CartiRepositoryInterface cartiRepositoryMock = new CartiRepositoryMock();
		CartiController cartiController = new CartiController(cartiRepositoryMock);
		Consola consola = new Consola(cartiController);
		try {
			consola.executa();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
