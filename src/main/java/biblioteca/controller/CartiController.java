package biblioteca.controller;


import biblioteca.model.Carte;
import biblioteca.repository.repositoryInterfaces.CartiRepositoryInterface;
import biblioteca.validator.ValidatorCarte;

import java.util.List;

public class CartiController {

	private CartiRepositoryInterface cartiRepositoryInterface;
	
	public CartiController(CartiRepositoryInterface cartiRepositoryInterface){
		this.cartiRepositoryInterface = cartiRepositoryInterface;
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		ValidatorCarte.validateCarte(c);
		cartiRepositoryInterface.adaugaCarte(c);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		ValidatorCarte.isStringOK(autor);
		return cartiRepositoryInterface.cautaCarte(autor);
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		if(!ValidatorCarte.isNumber(an))
			throw new Exception("Nu e numar!");
		return cartiRepositoryInterface.getCartiOrdonateDinAnul(an);
	}
	
	
}
