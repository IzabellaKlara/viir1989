package biblioteca.model;


import java.util.ArrayList;
import java.util.List;

public class Carte {
	
	private String titlu;
	private List<String> autori;
	private String editura;
	private String anAparitie;
	private List<String> cuvinteCheie;
	
	public Carte(){
		titlu = "";
		autori = new ArrayList<String>();
		anAparitie = "";
		editura = "";
		cuvinteCheie = new ArrayList<String>();
	}

	public String getEditura() {
		return editura;
	}

	public void setEditura(String editura) {
		this.editura = editura;
	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public List<String> getAutori() {
		return autori;
	}

	public void setAutori(List<String> ref) {
		this.autori = ref;
	}

	public String getAnAparitie() {
		return anAparitie;
	}

	public void setAnAparitie(String anAparitie) {
		this.anAparitie = anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) {
		this.cuvinteCheie = cuvinteCheie;
	}
	
	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaAutor(String autor){
		autori.add(autor);
	}
	
	@Override
	public String toString(){
		String autori = "";
		String cuvinteCheie = "";
		
		for(int i = 0; i< this.autori.size(); i++){
			if(i== this.autori.size()-1)
				autori+= this.autori.get(i);
			else
				autori+= this.autori.get(i)+",";
		}
		
		for(int i = 0; i< this.cuvinteCheie.size(); i++){
			if(i== this.cuvinteCheie.size()-1)
				cuvinteCheie+= this.cuvinteCheie.get(i);
			else
				cuvinteCheie+= this.cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+autori+";"+anAparitie+";"+cuvinteCheie;
	}
	
	public static Carte getCarteFromString(String carte){
		Carte carteCreata = new Carte();
		String []atr = carte.split(";");
		String []autori = atr[1].split(",");
		String []cuvinteCheie = atr[4].split(",");
		carteCreata.titlu=atr[0];
		for(String s:autori){
			carteCreata.adaugaAutor(s);
		}
		carteCreata.anAparitie = atr[2];
		carteCreata.editura = atr[3];
		for(String s:cuvinteCheie){
			carteCreata.adaugaCuvantCheie(s);
		}
		return carteCreata;
	}
	
}
